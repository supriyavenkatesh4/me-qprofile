
module.exports = {
  // TODO: change your profile information here
  name: "Supriya Venkatesh",
  greeting: "Hey 👋",
  greetingDescription: "I'm Supriya Venkatesh, an aspiring software engineer with previous internship experience in Artificial Intelligence and Robotics. ",
  githubUrl: "https://github.com/supriya-venkatesh",
  linkedinUrl: "https://www.linkedin.com/in/supriyavenkatesh99/",
  cvLink: "https://docs.google.com/document/d/1aNJUplAjbDO5gRS1LpdztPbLtIJ1Gstm/edit?usp=drive_web&ouid=116955512032176483460&rtpof=true",
};
